package { 'java-1.8.0-openjdk-devel':
  ensure => present,
}

exec { 'executando o jogovelha':
  command => 'java -jar jenkins.war &',
  path => '/usr/bin',
  ensure => running,
}
